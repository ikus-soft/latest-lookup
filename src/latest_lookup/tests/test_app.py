# -*- coding: utf-8 -*-
# latest-lookup, API to lookup latest version
# Copyright (C) 2021 IKUS Software inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cherrypy.test.helper
import responses

from latest_lookup.app import Root
from latest_lookup.config import parse_args


class TestApp(cherrypy.test.helper.CPWebCase):
    interactive = False

    @classmethod
    def setup_server(cls):
        cfg = parse_args([])
        app = Root(cfg)
        cherrypy.tree.mount(app)

    def test_index(self):
        # Given the application is started
        # When making a query to index page
        self.getPage("/")
        # Then an html page is returned
        self.assertStatus(200)
        self.assertInBody("OK")

    @responses.activate
    def test_minarca_latest_version(self):
        responses.add(
            responses.GET,
            "https://nexus.ikus-soft.com/service/rest/v1/search/assets",
            json={
                "items": [
                    {
                        "downloadUrl": "https://nexus.ikus-soft.com/repository/apt-release-bullseye/pool/m/minarca-client/minarca-client_6.0.3_amd64.deb",
                        "path": "pool/m/minarca-client/minarca-client_6.0.3_amd64.deb",
                        "id": "YXB0LXJlbGVhc2UtYnVsbHNleWU6MTNiMjllNDQ5ZjBlM2I4ZDM0ZWIyNmU0NTg5MmMwZjk",
                        "repository": "apt-release-bullseye",
                        "format": "apt",
                        "checksum": {
                            "sha1": "30d01a0ac9cbe582cb2e578fc299f428f1953ee8",
                            "sha256": "e979fa0d13cd773249cab5a859c71460b3f9751ada1b65fcb7e64ff35d720409",
                            "md5": "8c067d73345fd681f41ca62e3466d017",
                        },
                        "contentType": "application/x-debian-package",
                        "lastModified": "2024-11-27T20:39:52.311+00:00",
                        "lastDownloaded": "2024-12-03T15:42:02.567+00:00",
                        "uploader": "deployment",
                        "uploaderIp": "10.255.10.28",
                        "fileSize": 63143130,
                    }
                ]
            },
            status=200,
        )
        # Given the application is started
        # When making a query to sender page
        self.getPage("/minarca/latest_version")
        # Then a default sender name is returned
        self.assertStatus(200)
        self.assertBody("6.0.3")
