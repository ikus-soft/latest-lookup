# -*- coding: utf-8 -*-
# latest-lookup, API to lookup latest version
# Copyright (C) 2021 IKUS Software inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

import cherrypy
import requests

logger = logging.getLogger(__name__)

COMPONENTS = {
    "minarca": "minarca-client",
    "minarca-client": "minarca-client",
    "minarca-server": "minarca-server",
    "rdiffweb": "rdiffweb",
}


@cherrypy.popargs("component")
@cherrypy.tools.caching()
@cherrypy.tools.proxy(local=None, remote='X-Real-IP')
class Root(object):
    """
    Root entry point exposed using cherrypy.
    """

    def __init__(self, cfg):
        self.cfg = cfg

    @cherrypy.expose
    def index(self):
        return "OK"

    @cherrypy.expose
    def latest_version(self, component):
        """
        Get Latest version
        """
        # Handle case of minarca server which might differ
        user_agent = cherrypy.request.headers.get('User-Agent', 'Unknown')
        if 'minarca-server' in user_agent.lower():
            component = 'minarca-server'

        if component not in COMPONENTS:
            raise cherrypy.HTTPError(status=404)

        # Query nexus server for latest artefacts
        ret = requests.get(
            "https://nexus.ikus-soft.com/service/rest/v1/search/assets",
            params={
                "name": COMPONENTS.get(component),
                "format": "apt",
                "repository": "apt-release-*",
                "sort": "version",
                "direction": "desc",
            },
        )
        ret.raise_for_status()
        data = ret.json()
        # From the list of items, extract the latest version.
        items = data.get("items", [])
        if not items:
            # Not Found
            raise cherrypy.HTTPError(status=404)
        # path == pool/m/minarca-client/minarca-client_6.0.3_amd64.deb"
        # path == pool/r/rdiffweb/rdiffweb_2.9.5+dfsg-1debian12_all.deb
        item_path = items[0].get("path")
        parts = item_path.split('_')
        if len(parts) < 3:
            raise cherrypy.HTTPError(status=404)
        version = parts[-2]
        # Strip everythin after '+'
        if '+' in version:
            version = version.split('+')[0]
        return version
