FROM python:3.9

# Allow app to listen on all interface
ENV LATEST_LOOKUP_SERVER_HOST=0.0.0.0
ENV LATEST_LOOKUP_SERVER_PORT=8080

EXPOSE 8080

# Install project and run test
WORKDIR /tmp
COPY . .
RUN pip install . .[test] && pytest

CMD [ "python", "/usr/local/bin/latest-lookup" ]
